package ce1;

import java.util.Arrays;
import java.util.Collection;
import java.util.EmptyStackException;
import java.util.Iterator;

public class ArrayStack implements Stack,Iterator {

  private int index = -1;

  private static final int CAPACITY = 15;

  private Object[] array;

  public ArrayStack() {
    this.array = new Object[CAPACITY];
  }

  @Override
  public void push(Object o) {
    if ((index + 1) == array.length) {
      resizeArray();
    }
    array[index + 1] = o;
    index++;
  }

  private void resizeArray() {
    array = Arrays.copyOf(array, CAPACITY * 2);
  }

  @Override
  public Object pop() {
    if (isEmpty()) {
      throw new EmptyStackException();
    }
    return array[index--];
  }

  @Override
  public Object get(int position) {
    if (isEmpty() || (position) > size()) {
      throw new IndexOutOfBoundsException("Index out of bounds");
    }

    return array[position];
  }

  @Override
  public int size() {
    return index + 1;
  }

  @Override
  public boolean isEmpty() {
    return index == -1;
  }

  @Override
  public boolean contains(Object o) {
    while (!isEmpty()) {
      if (array[index].equals(o)) {
        return true;
      }
      index--;
    }

    return false;
  }

  @Override
  public void clear() {
    array[index] = null;
  }

  @Override
  public Iterator iterator() {
    return this;
  }

  @Override
  public Object[] toArray() {
    return new Object[0];
  }

  @Override
  public Object[] toArray(Object[] a) {
    return new Object[0];
  }

  @Override
  public boolean add(Object o) {
    return false;
  }

  /************************************************************/
  @Override
  public boolean remove(Object o) {
    return false;
  }

  @Override
  public boolean addAll(Collection c) {
    return false;
  }

  @Override
  public boolean retainAll(Collection c) {
    return false;
  }

  @Override
  public boolean removeAll(Collection c) {
    return false;
  }

  @Override
  public boolean containsAll(Collection c) {
    return false;
  }

  @Override
  public boolean hasNext() {
    if (!isEmpty()){
      return true;
    }
    return false;
  }

  @Override
  public Object next() {
    if (hasNext()){
      return array[index--];
    }
    return null;
  }
}
