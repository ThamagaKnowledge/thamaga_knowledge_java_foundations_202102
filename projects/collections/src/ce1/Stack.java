package ce1;

import java.util.Collection;
import java.util.Iterator;

public interface Stack extends Collection {

  void push(Object o);

  Object pop();

  Object get(int position);

  @Override
  int size();

  @Override
  boolean isEmpty();

  @Override
  boolean contains(Object o);

  @Override
  Iterator iterator();

  @Override
  boolean add(Object o);
}
