package ce1;

import java.util.Iterator;

public class ArrayStackUtils {

  public static void main(String[] args) {

    ArrayStack arrayStack = new ArrayStack();

    arrayStack.push("First element to be added");
    arrayStack.push(2);
    arrayStack.push(3);
    arrayStack.push(4);
    arrayStack.push(5);
    arrayStack.push("Last Element to Be Added");

    Iterator iterator = arrayStack.iterator();

    while (iterator.hasNext()){
      System.out.println("iterator.next() = " + iterator.next());
    }

    System.out.println("arrayStack.size() = " + arrayStack.size());
    System.out.println("arrayStack.isEmpty() = " + arrayStack.isEmpty());
    System.out.println("arrayStack.get(1) = " + arrayStack.get(3));
    System.out.println("arrayStack.contains(4) = " + arrayStack.contains(0));
////    arrayStack.clear();
//    System.out.println("arrayStack.pop() = " + arrayStack.pop());
  }
}
