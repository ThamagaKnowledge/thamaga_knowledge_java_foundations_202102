package hw4;

import java.util.Iterator;

public class LinkedListUtils {

  public static void main(String[] args) {

    LinkedList myList = new LinkedList();

    myList.add("1st Node");
    myList.add("2nd Node");
    myList.add("3rd Node");
    myList.add("4th Node");
    myList.add("5th Node");

    System.out.println("myList.remove(0) = " + myList.remove(2));


//    System.out.println("myList.contains(\"2nd Node\") = " + myList.contains("2nd Node"));
//    System.out.println("myList.get(2) = " + myList.get(4));
//    System.out.println("myList.lastIndexOf(\"5th Node\") = " + myList.lastIndexOf("5th Node"));
//
//    System.out.println("myList.indexOf(\"2nd Node\") = " + myList.indexOf("5th Node"));
//    System.out.println("myList.get(1) = " + myList.get(4));
//    myList.add(0,"Second Added Element");
////    myList.clear();
//      System.out.println("myList.indexOf(\"3rd Node\") = " + myList.indexOf("3rd Node"));

    Iterator iterator = myList.iterator();

    while (iterator.hasNext()) {
      System.out.println("iterator.next() = " + iterator.next());
    }

  }

}
