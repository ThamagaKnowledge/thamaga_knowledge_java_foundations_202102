package hw4;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class LinkedList implements List, Iterator {

  private int size = 0;

  private Node head; //Head Node

  private Node node;

  // private Node next; //Pointer to the next Node

  public LinkedList() {
    //This is an empty List, so the reference of the head
    //is set to the node with no data.
    this.head = null;
  }

  @Override
  public int size() {
    Node currentPointer = head;

    while (currentPointer != null) {
      currentPointer = currentPointer.getNext();
      size++;
    }
    return size;
  }

  @Override
  public boolean isEmpty() {
    return head == null;
  }

  @Override
  public boolean contains(Object o) {
    Node currentPointer = head;

    while (currentPointer != null){
      if (currentPointer.getData().equals(o)){
        return true;
      }
      currentPointer = currentPointer.getNext();
    }
    return false;
  }

  @Override
  public Iterator iterator() {
    return this;
  }

  @Override
  public Object[] toArray() {
    return new Object[0];
  }

  @Override
  public boolean add(Object o) {
    //Appends the element/node at the end of the List.

    node = new Node(o);
    node.getNext();

    //If the List is empty, then make the newList as Head
    if (head == null) {
      head = node;
    } else {
      //Traverse to the end of the List
      Node currentPointer = head;
      while (currentPointer.getNext() != null) {
        currentPointer = currentPointer.getNext();
      }
      currentPointer.setNext(node);
    }

    //Return true to indicate the new Node has been added.
    return true;
  }

  @Override
  public boolean remove(Object o) {

    return false;
  }

  @Override
  public boolean addAll(Collection c) {
    return false;
  }

  @Override
  public boolean addAll(int index, Collection c) {
    return false;
  }

  @Override
  public void clear() {
    head = null;
  }

  @Override
  public Object get(int index) {
    if (index < 0) {
      return null;
    }

    Node currentPointer = head;
    if (head != null) {
      for (int i = 0; i < index; i++) {
        if (currentPointer == null) {
          return null;
        }
        currentPointer = currentPointer.getNext();
      }
      return currentPointer.getData();
    }

    return currentPointer.getData().toString();
  }

  @Override
  public Object set(int index, Object element) {
    return null;
  }

  @Override
  public void add(int index, Object element) {
    Node newNode = new Node(element);
    newNode.getNext();

    Node currentPointer = head;
    if (currentPointer != null) {

      for (int i = 0; i < (index - 1) && (currentPointer.getNext() != null); i++) {
        currentPointer = currentPointer.getNext();
      }
    }
    newNode.setNext(currentPointer.getNext());
    currentPointer.setNext(newNode);
  }

  @Override
  public Object remove(int index) {

    if (index < 0 || index > size()){
      return false;
    }

    Node currentPointer = head;

    if (head != null){
      for (int i = 0; i < index; i++){
        if (currentPointer.getNext() == null){
          currentPointer.setNext(null);
        }
        currentPointer = currentPointer.getNext();
      }
    }
    return currentPointer.getData();
  }

  @Override
  public int indexOf(Object o) {
    Node currentPointer = head;
    int index = 0;

    while(currentPointer != null){
      if (currentPointer.getData().equals(o)){
        return index;
      }
      currentPointer = currentPointer.getNext();
      index++;
    }

    return -1;
  }

  @Override
  public int lastIndexOf(Object o) {
    int index = -1;
    Node currentPointer =  head;
    while (currentPointer != null){
      index++;
      currentPointer = currentPointer.getNext();
    }

    return index;
  }

  @Override
  public ListIterator listIterator() {
    return null;
  }

  @Override
  public ListIterator listIterator(int index) {
    return null;
  }

  @Override
  public List subList(int fromIndex, int toIndex) {
    return null;
  }

  @Override
  public boolean retainAll(Collection c) {
    return false;
  }

  @Override
  public boolean removeAll(Collection c) {
    return false;
  }

  @Override
  public boolean containsAll(Collection c) {
    return false;
  }

  @Override
  public Object[] toArray(Object[] a) {
    return new Object[0];
  }

  @Override
  public boolean hasNext() {
    if (head == null) {
      return false;
    } else {
      return true;
    }
  }

  @Override
  public Object next() {

    Object data = head.getData();
    head = head.getNext();

    return data;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (this.getClass() != obj.getClass()) {
      return false;
    }

    Node data = (Node) obj;
    return data.equals(obj);

  }
}
