package hw1;

import java.util.Set;
import java.util.TreeSet;

public class PrintCustomerNum {

  public static void main(String[] args) {

printCustomerInAsc(createCustomer());
printCustomerInDsc(createCustomerDesc());

  }

  private static Set createCustomer() {

    Set mySet = new TreeSet();

    CustomerAsc customerAsc = new CustomerAsc("129", "Sao", "Local");
    CustomerAsc customerAsc1 = new CustomerAsc("121", "Su", "Local");
    CustomerAsc customerAsc2 = new CustomerAsc("127", "Yen", "Local");
    CustomerAsc customerAsc3 = new CustomerAsc("123", "Ru", "Local");
    CustomerAsc customerAsc4 = new CustomerAsc("122", "Saki", "Local");

    mySet.add(customerAsc);
    mySet.add(customerAsc1);
    mySet.add(customerAsc2);
    mySet.add(customerAsc3);
    mySet.add(customerAsc4);

    return mySet;

  }

  private static Set createCustomerDesc() {

    Set mySet = new TreeSet();

    CustomerDesc customerDesc = new CustomerDesc("129", "Sao", "Local");
    CustomerDesc customerDesc1 = new CustomerDesc("121", "Su", "Local");
    CustomerDesc customerDesc2 = new CustomerDesc("127", "Yen", "Local");
    CustomerDesc customerDesc3 = new CustomerDesc("123", "Ru", "Local");
    CustomerDesc customerDesc4 = new CustomerDesc("122", "Saki", "Local");

    mySet.add(customerDesc);
    mySet.add(customerDesc1);
    mySet.add(customerDesc2);
    mySet.add(customerDesc3);
    mySet.add(customerDesc4);

    return mySet;

  }

  public static void printCustomerInAsc(Set customer){
    System.out.println("Customer Id's in Ascending: ");
    for (Object o : customer) {
      System.out.println(((CustomerAsc) o).printCustomer());
    }
  }

  private static void printCustomerInDsc(Set customer) {
    System.out.println("Customer Id's in Descending: ");
    for (Object o : customer) {
      System.out.println(((CustomerDesc) o).printCustomer());
    }
  }

}
