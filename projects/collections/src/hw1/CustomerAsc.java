package hw1;

public class CustomerAsc implements Comparable {

  private String customerId;

  private String customerName;

  private String customerType;

  public CustomerAsc(String customerId, String customerName, String customerType) {
    this.customerId = customerId;
    this.customerName = customerName;
    this.customerType = customerType;
  }


  public String printCustomer() {
    return
        "customerId = " + customerId;
  }

  @Override
  public int compareTo(Object o) {
    return this.customerId.compareTo(((CustomerAsc) o).customerId);
  }
}
