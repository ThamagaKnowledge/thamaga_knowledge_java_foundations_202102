package hw1;

public class CustomerDesc implements Comparable {

  private String customerId;

  private String customerName;

  private String customerType;

  public CustomerDesc(String customerId, String customerName, String customerType) {
    this.customerId = customerId;
    this.customerName = customerName;
    this.customerType = customerType;
  }

  public String printCustomer() {
    return
        "customerId = " + customerId;
  }

  @Override
  public int compareTo(Object o) {
    return -1 * this.customerId.compareTo(((CustomerDesc) o).customerId);
  }
}
