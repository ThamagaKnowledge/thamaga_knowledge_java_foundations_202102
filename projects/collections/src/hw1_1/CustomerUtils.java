package hw1_1;

import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

public class CustomerUtils {

  public static void main(String[] args) {
    SortedSet customerSet = new TreeSet(new PrintCustomerInAscOrder());
    SortedSet customerSet1 = new TreeSet(new PrintCustomerInDescOrder());
    printEmployee(customerSet(customerSet));
    System.out.println("\n");
    printEmployee(customerSet(customerSet1));
  }

  public static SortedSet customerSet(SortedSet customerSet) {

    Customer customer = new Customer("124", "Su", 26);
    Customer customer1 = new Customer("129", "Saki", 34);
    Customer customer2 = new Customer("127", "Den", 28);
    Customer customer3 = new Customer("125", "Ru", 53);
    Customer customer4 = new Customer("123", "Suki", 33);

    customerSet.add(customer);
    customerSet.add(customer1);
    customerSet.add(customer2);
    customerSet.add(customer3);
    customerSet.add(customer4);

    return customerSet;
  }

  public static void printEmployee(SortedSet customerSet) {

    for (Object o : customerSet) {
      System.out.println(((Customer) o).printCustomer());
    }
  }
}
