package hw1_1;

public class Customer extends PrintCustomerInAscOrder {

  private String customerNum;

  private String customerName;

  private int customerAge;

  public Customer(String customerNum, String customerName, int customerAge) {
    this.customerNum = customerNum;
    this.customerName = customerName;
    this.customerAge = customerAge;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public String printCustomer() {
    return
        "employeeNum = " + customerNum +
            ", employeeName = " + customerName +
            ", employeeAge = " + customerAge;

  }

}
