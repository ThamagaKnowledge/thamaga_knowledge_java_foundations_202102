package hw1_1;

import java.util.Comparator;

public class PrintCustomerInAscOrder implements Comparator {
  @Override
  public int compare(Object o1, Object o2) {
    return ((Customer)o1).getCustomerNum().compareTo(((Customer)o2).getCustomerNum());
  }
}
