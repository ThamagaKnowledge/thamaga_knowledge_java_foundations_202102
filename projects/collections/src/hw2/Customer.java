package hw2;

public class Customer {

  private String customerId;

  private String customerName;

  private String customerType;

  private double customerAnnualSalary;

  public Customer(String customerId, String customerName, String customerType, double customerAnnualSalary) {
    this.customerId = customerId;
    this.customerName = customerName;
    this.customerType = customerType;
    this.customerAnnualSalary = customerAnnualSalary;
  }

  public double getCustomerAnnualSalary() {
    return customerAnnualSalary;
  }

  @Override
  public String toString() {
    return "Customer{" +
        "customerId='" + customerId + '\'' +
        ", customerName='" + customerName + '\'' +
        ", customerType='" + customerType + '\'' +
        ", customerAnnualSalary=" + customerAnnualSalary +
        '}';
  }
}
