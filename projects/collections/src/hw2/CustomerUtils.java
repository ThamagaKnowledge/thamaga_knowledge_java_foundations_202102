package hw2;

import java.util.HashMap;
import java.util.Map;

public class CustomerUtils {

  public static void main(String[] args) {

    Customer customer = new Customer("122", "Su", "Local", 100000);
    Customer customer1 = new Customer("123", "Saki", "Local", 2500000);
    Customer customer2 = new Customer("124", "Yen", "Local", 13600000);
    Customer customer3 = new Customer("127", "Ru", "Local", 1500000);
    Customer customer4 = new Customer("121", "Den", "Local", 177000);

    myMap.put(customer, customer.getCustomerAnnualSalary());
    myMap.put(customer1, customer1.getCustomerAnnualSalary());
    myMap.put(customer2, customer2.getCustomerAnnualSalary());
    myMap.put(customer3, customer3.getCustomerAnnualSalary());
    myMap.put(customer4, customer4.getCustomerAnnualSalary());

    printCustomerAnnualSalary(customer3);
  }
  static Map myMap = new HashMap();

  public static void printCustomerAnnualSalary(Customer customer){
    System.out.println("customer = " + myMap.get(customer));
  }
}
