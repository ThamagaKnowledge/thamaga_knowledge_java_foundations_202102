package hw3;

import java.util.Comparator;

public class ReverseStrings implements Comparator {
  @Override
  public int compare(Object o1, Object o2) {
    return ((String)o2).compareTo((String) o1);
  }
}
