package hw3;

import java.util.SortedSet;
import java.util.TreeSet;

public class UniqueStrings {

  public static void main(String[] args) {
    takeUniqueString();
  }


  public static void takeUniqueString(){

    SortedSet mySet = new TreeSet(new ReverseStrings());

    String str1 = "Banana";
    String str2 = "Apple";
    String str3 = "Orange";
    String str4 = "Peach";
    String str5 = "Mango";

    mySet.add(str1);
    mySet.add(str2);
    mySet.add(str3);
    mySet.add(str4);
    mySet.add(str5);


    System.out.println(mySet);
  }

}

