package com.pysbergate.grade2021.core.innerclasses.homeworks.hw2;

public class ObjectInnerClass {

 public void methodInner(){

   class Inner{
     public void innerMethod(){
       System.out.println("I am the inner method: ");
     }
   }

   Inner inner = new Inner();
   inner.innerMethod();
 }
}
