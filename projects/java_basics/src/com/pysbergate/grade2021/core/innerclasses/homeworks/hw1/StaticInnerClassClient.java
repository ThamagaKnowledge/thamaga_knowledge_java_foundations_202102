package com.pysbergate.grade2021.core.innerclasses.homeworks.hw1;

public class StaticInnerClassClient {

  public static void main(String[] args) {
    StaticInnerClass.innerClass innerClass = new StaticInnerClass.innerClass();
    innerClass.printInInner();
  }
}
