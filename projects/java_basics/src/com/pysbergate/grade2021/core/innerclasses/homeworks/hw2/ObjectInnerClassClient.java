package com.pysbergate.grade2021.core.innerclasses.homeworks.hw2;

public class ObjectInnerClassClient {

  public static void main(String[] args) {
    ObjectInnerClass obj = new ObjectInnerClass();
    obj.methodInner();
  }
}
