package com.pysbergate.grade2021.core.refelction.ce2;

import com.pysbergate.grade2021.core.refelction.ce1.Student;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class DynamicLogicOnStudentClass {

  public static void main(String[] args) {

    try {

      //Get the class Constructors.
      Constructor<? extends Student> studConstructor = Student.class.getConstructor();
      Constructor<? extends Student> stud1Constructor =
          Student.class.getConstructor(String.class, String.class, String.class,
              String.class, String.class);

      Student studentNewInstance = studConstructor.newInstance();
      Student student1NewInstance = stud1Constructor.newInstance("Sadio", "Mane", "19-03-1990", "Liverpool",
          "Male");

      studentNewInstance.displayMessage();
      student1NewInstance.displayMessage();

    } catch (InstantiationException e) {
      e.printStackTrace();
    } catch (InvocationTargetException e) {
      e.printStackTrace();
    } catch (NoSuchMethodException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }
  }
}