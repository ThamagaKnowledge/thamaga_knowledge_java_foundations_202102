package com.pysbergate.grade2021.core.refelction.hw1;

import com.pysbergate.grade2021.core.refelction.ce1.Student;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class BasicReflectionOnStudentClass {

  public static void main(String[] args) {
    Class studentClass = Student.class;

    //Show the name of the Superclass
    System.out
        .println("studentClass.getSuperclass().getSimpleName() = " + studentClass.getSuperclass().getSimpleName());

    //Show all the Modifiers of the class Member\
    int modifiers = studentClass.getModifiers();
    String classModifier = Modifier.toString(modifiers);
    System.out.println(studentClass.getSimpleName() + " class is " + classModifier + "\n");

    Field[] fields = studentClass.getDeclaredFields();

    for (Field field : fields) {
      System.out.println(field.getName() + " is " + Modifier.toString(field.getModifiers()));
    }
    System.out.println("\n");

    Method[] methods = studentClass.getDeclaredMethods();

    for (Method method : methods) {
      System.out.println(method.getName() + " is " + Modifier.toString(method.getModifiers()));
    }

    System.out.println("\n");

    for (Method method : methods) {
      System.out.println(method.getName() + " returns " + method.getReturnType().getSimpleName());
    }
  }
}
