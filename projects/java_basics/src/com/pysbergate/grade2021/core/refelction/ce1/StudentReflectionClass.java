package com.pysbergate.grade2021.core.refelction.ce1;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class StudentReflectionClass {

  private static Constructor[] constructors;

  public static void main(String[] args) {

    Class student = Student.class;

    //Get the class name
    String className = student.getSimpleName();
    System.out.println("className = " + className);

    //Get the Package name
    String packageName = student.getPackageName();
    System.out.println("packageName = " + packageName);

    //Get the declared Annotations
    Annotation[] annotations = student.getDeclaredAnnotations();
    for (Annotation annotation : annotations) {
      System.out.println("annotation = " + annotation);
    }

    //Get Interfaces
    Class[] classInterfaces = student.getInterfaces();
    for (Class classInterface : classInterfaces) {
      System.out.println("classInterface = " + classInterface);
    }

    //Get name of Public fields
    Field[] fields = student.getDeclaredFields();

    for (Field field : fields) {
      if (field.getModifiers() == 1) {
        System.out.println("field = " + field.getName());
      }
    }
    System.out.println("\n");

    for (Field field : fields) {
      System.out.println("field.getName() = " + field.getName());
    }
    System.out.println("\n");

    //Get Constructor's Parameter List
    Constructor[] constructors = student.getDeclaredConstructors();

    for (Constructor constructor : constructors) {
      System.out.println("constructor.getName() = " + constructor.getName() + "\n");

      Parameter[] parameters = constructor.getParameters();

      for (Parameter parameter : parameters) {
        System.out.println("parameter.getParameterizedType() = " + parameter.getParameterizedType() + "\n");
        System.out.println("parameter.getName() = " + parameter.getName() + "\n");
      }
    }

    //Get the Methods names
    Method[] methods = student.getDeclaredMethods();

    for (Method method : methods) {
      System.out.println("method.getName() = " + method.getName());
    }

  }
}
