package com.pysbergate.grade2021.core.refelction.ce1;

import com.pysbergate.grade2021.core.annotations.hw2.DomainClass;

import java.io.Serializable;

@DomainClass
public class Student implements Serializable{

  private String name;

  private String surname;

  private String dateOfBirth;

  private String address;

  public String gender;

  public Student() {
  }

  public Student(String name, String surname, String dateOfBirth, String address) {
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
    this.address = address;
  }

  public Student(String name, String surname, String dateOfBirth, String address, String gender) {
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
    this.address = address;
    this.gender = gender;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public void displayMessage(){
    System.out.println("I am Computer Science Student");
  }

}
