package com.pysbergate.grade2021.core.annotations.hw3Part2;

import com.pysbergate.grade2021.core.annotations.hw2.Customer;
import com.pysbergate.grade2021.core.annotations.hw2.DomainProperty;
import org.w3c.dom.stylesheets.LinkStyle;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class CustomerService {

  public static void main(String[] args) {
//    saveCustomer();
//    connectToDB();
    System.out.println("insertIntoTable() = " + insertIntoTable());
  }

  public static String insertIntoTable() {
    Field[] fields = Customer.class.getDeclaredFields();

    String sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "
        + "VALUES (1, 'Paul', 32, 'California', 20000.00 );";

    StringBuilder sqlStatement = new StringBuilder();
    sqlStatement.append("insert into customer (");
    for (Field field : fields) {
      Annotation[] annotations = field.getDeclaredAnnotations();
      for (Annotation annotation : annotations) {
        if (annotation instanceof DomainProperty) {
          sqlStatement.append(field.getName() + " ");

        }
      }
    }
    sqlStatement.append(")");
//    System.out.println("sqlStatement = " + sqlStatement);
    sqlStatement.append(" values (");
    for (Field field : fields) {
      Annotation[] annotations = field.getDeclaredAnnotations();
      for (Annotation annotation : annotations) {
        if (annotation instanceof DomainProperty) {

          sqlStatement.append(saveCustomer());

          break;
        }
      }
    }
    return sqlStatement.toString();
  }

  public static String generateRandomString() {
    int leftLimit = 48; // numeral '0'
    int rightLimit = 122; // letter 'z'
    int targetStringLength = 10;
    Random random = new Random();

    String generatedString = random.ints(leftLimit, rightLimit + 1)
        .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
        .limit(targetStringLength)
        .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
        .toString();

    return generatedString;
//    System.out.println(generatedString);
  }

  public static List<Object> saveCustomer() {

    List<Object> values = new ArrayList<>();
    List<Customer> customers = new ArrayList<>();

    customers.add(new Customer(generateRandomString(), generateRandomString()
        , generateRandomString(), LocalDate.now().toString()));

//    List<Customer> customers = Arrays.asList(new Customer(generateRandomString(), generateRandomString()
//            , generateRandomString(), LocalDate.now().toString())
////        new Customer(generateRandomString(), generateRandomString()
////            , generateRandomString(), LocalDate.now().toString()),
////        new Customer(generateRandomString(), generateRandomString()
////            , generateRandomString(), LocalDate.now().toString()),
////        new Customer(generateRandomString(), generateRandomString()
////            , generateRandomString(), LocalDate.now().toString())
//    );

      values.add("'" + customers.get(0).getCustomerNum() + "'" + ", " + "'" +
          customers.get(1).getName() + "'" + ", " + "'" + customers.get(2).getSurname() + "'" +
          ", " + "'" + customers.get(3).getDateOfBirth() + "'");

    return values;
  }

  public static void connectToDB() {
    Connection c = null;
    Statement stmt = null;
    try {
      Class.forName("org.postgresql.Driver");
      c = DriverManager
          .getConnection("jdbc:postgresql://localhost:5432/",
              "postgres", "Big4X#$%T");
      c.setAutoCommit(false);
      System.out.println("Opened database successfully");

      stmt = c.createStatement();
      String sql = insertIntoTable();
      stmt.executeUpdate(sql);

      stmt.close();
      c.commit();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Records created successfully");
  }
}
