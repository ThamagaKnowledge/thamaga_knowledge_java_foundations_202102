package com.pysbergate.grade2021.core.annotations.hw3;

import com.pysbergate.grade2021.core.annotations.hw2.Customer;
import com.pysbergate.grade2021.core.annotations.hw2.DomainProperty;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class DataBaseManager {
  public static void main(String[] args) throws NoSuchFieldException {
    createTable();
  }

  public static String generateDatabase() throws NoSuchFieldException {
    Field[] fields = Customer.class.getDeclaredFields();

    StringBuilder sqlStatement = new StringBuilder();
    sqlStatement.append("create table customer (");
    for (Field field : fields) {
      Annotation[] annotations = field.getDeclaredAnnotations();
      for (Annotation annotation : annotations) {
        if (annotation instanceof DomainProperty) {
          sqlStatement.append(field.getName());
          if (field.getType().getSimpleName().equals("String")) {
            sqlStatement.append(" varchar, ");
          } else {
            sqlStatement.append(" int, ");
          }
          break;
        }
      }
    }
    String str = sqlStatement.toString();
    String b = str.replaceAll(", $", "");
    StringBuilder stringBuilder = new StringBuilder(b);
    stringBuilder.append(")");
    String finalString = stringBuilder.toString();
    System.out.println("sqlStatement = " + finalString);
    return finalString;
  }

  public static void createTable() {
    Connection c = null;
    Statement stmt = null;
    try {
      Class.forName("org.postgresql.Driver");
      c = DriverManager
          .getConnection("jdbc:postgresql://localhost:5432/postgres",
              "postgres", "Big4X#$%T");
      System.out.println("Opened database successfully");

      stmt = c.createStatement();
//      String sql = "CREATE TABLE CUSTOMER " +
//          "(ID INT PRIMARY KEY     NOT NULL," +
//          " CUSTOMERNUM           TEXT    NOT NULL, " +
//          " NAME            TEXT     NOT NULL, " +
//          " SURNAME        TEXT, " +
//          " DATE_OF_BIRTH         TEXT     NOT NULL, " +
//          " AGE            INT     NOT NULL)";

      String sql = generateDatabase();
      stmt.executeUpdate(sql);
      stmt.close();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Table created successfully");
  }
}
