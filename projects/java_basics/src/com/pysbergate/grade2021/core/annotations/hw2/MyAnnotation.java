package com.pysbergate.grade2021.core.annotations.hw2;

import java.lang.annotation.*;

/**
 * @since 19 Jul 2010
 */
@Retention(RetentionPolicy.RUNTIME)
// if this is not here, will not be available at Runtime (cannot be accessed via reflection)
@Target({ElementType.TYPE, ElementType.CONSTRUCTOR, ElementType.METHOD, ElementType.FIELD,
    ElementType.PARAMETER})
public @interface MyAnnotation {

  String someValue1() default "Johan";

  int someValue2();

  String[] someValues3() default {"abc"};
}
