package com.pysbergate.grade2021.core.annotations.hw2;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface DomainProperty {

  String field() default "";

  boolean primary_key() default false;

  boolean nullable() default true;

  boolean unique() default false;

}
