package com.pysbergate.grade2021.core.annotations.hw3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class CreateTable {

  public static void CreateTable() {
    Connection c = null;
    Statement stmt = null;
    try {
      Class.forName("org.postgresql.Driver");
      c = DriverManager
          .getConnection("jdbc:postgresql://localhost:5432/postgres",
              "postgres", "Big4X#$%T");
      System.out.println("Opened database successfully");

      stmt = c.createStatement();
      String sql = "CREATE TABLE CUSTOMER " +
          "(ID INT PRIMARY KEY     NOT NULL," +
          " CUSTOMERNUM           TEXT    NOT NULL, " +
          " NAME            TEXT     NOT NULL, " +
          " SURNAME        TEXT, " +
          " DATE_OF_BIRTH         TEXT     NOT NULL, " +
          " AGE            INT     NOT NULL)";
      stmt.executeUpdate(sql);
      stmt.close();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Table created successfully");
  }
}
