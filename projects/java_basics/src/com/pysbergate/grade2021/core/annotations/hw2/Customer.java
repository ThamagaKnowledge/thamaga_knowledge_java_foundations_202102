package com.pysbergate.grade2021.core.annotations.hw2;

@DomainClass
public class Customer {

  @DomainProperty(field = "CustomerNum", primary_key = true, nullable = false, unique = true)
  private String customerNum;

  @DomainProperty(field = "name")
  private String name;

  @DomainProperty(field = "surname")
  private String surname;

  @DomainProperty(field = "dateOfBirth")
  private String dateOfBirth;

  @DomainTransient(age = 0)
  private int age;

  public Customer(String customerNum, String name, String surname, String dateOfBirth, int age) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
    this.age = age;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public int getAge() {
    return age;
  }
}
