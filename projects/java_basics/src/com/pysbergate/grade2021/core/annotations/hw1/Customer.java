package com.pysbergate.grade2021.core.annotations.hw1;

public class Customer {

  private String customerNum;

  private String name;

  private String surname;

  private String dateOfBirth;

  private int age;

  public Customer(String customerNum, String name, String surname, String dateOfBirth, int age) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
    this.age = age;
  }

}
