package com.pysbergate.grade2021.core.concurrency.hw1a_v4;

import java.util.ArrayList;
import java.util.List;

public class Messages {

  private List<String> messages = new ArrayList<>();

  private boolean writeMessage = false;

  public synchronized void write() {
    System.out.println("writing a message...");
    String msg = null;

    while (writeMessage) {
      try {
        wait();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    for (int i = 0; i <= 10; i++) {
      msg = "message : " + i;
      messages.add(msg);
    }
    System.out.println("completed writing messages....");
    writeMessage = true;
    notify();
  }

  public synchronized void read() {
    System.out.println("reading messages...");
    String result = null;

    while (!writeMessage) {
      try {
        wait();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    writeMessage = false;
    notify();
    for (String message : messages) {
      result = message;
      System.out.println(result);
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

}
