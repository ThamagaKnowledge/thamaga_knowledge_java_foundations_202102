package com.pysbergate.grade2021.core.concurrency.hw1a_v4;

public class MessageClient {

  public static void main(String[] args) {

    Messages messages = new Messages();

    Thread writer = new Thread(new MessageWriter(messages));
    Thread reader = new Thread(new MessageReader(messages));
    writer.start();
    reader.start();

  }
}
