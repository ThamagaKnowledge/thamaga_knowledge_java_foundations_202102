package com.pysbergate.grade2021.core.concurrency.hw1a_v4;

public class MessageWriter implements Runnable{

  private Messages messages;

  public MessageWriter(Messages messages) {
    this.messages = messages;
  }


  @Override
  public void run() {
    messages.write();
  }
}
