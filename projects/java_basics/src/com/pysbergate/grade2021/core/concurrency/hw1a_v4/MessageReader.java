package com.pysbergate.grade2021.core.concurrency.hw1a_v4;

import java.util.List;

public class MessageReader implements Runnable {

  private Messages messages;

  private boolean checkMessages = true;

  public MessageReader(Messages messages) {
    this.messages = messages;
  }

  @Override
  public void run() {
    messages.read();
  }
}
