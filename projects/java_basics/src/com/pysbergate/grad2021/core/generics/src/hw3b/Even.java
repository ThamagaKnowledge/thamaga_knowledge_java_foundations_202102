package hw3b;

public class Even<T> implements GenericCheck<Integer> {

  @Override
  public boolean check(Integer element) {
    return (element % 2 == 0);
  }
}
