package hw3b;

public class StringStartingWithC<T> implements GenericCheck<String> {

  @Override
  public boolean check(String element) {
    if (element.charAt(0) != 'C') {
      return false;
    }
    return true;
  }
}
