package hw3a_ver3;

import java.util.Arrays;
import java.util.Collection;

public class CollectionOfTypes {

  public static void main(String[] args) {
    Collection<Integer> integers = Arrays.asList(2, 6, 7, 23, 54, 78, 879, 4, 67, 45);
    Collection<String> strings = Arrays.asList("Cable", "Calculator", "Bin", "Book", "Chisel");

    Collection<Account> accounts = Arrays.asList(new Account(123, 450),
        new Account(124, -15), new Account(125, -990));

    System.out.println("countEventNumbers(integers) = " + countEventNumbers(integers));
    System.out.println("countOddNumbers(integers) = " + countOddNumbers(integers));
    System.out.println("countPrimeIntegers(integers) = " + countPrimeIntegers(integers));
    System.out.println("countStringStartingWithC(strings) = " + countStringStartingWithC(strings));
    System.out.println("countAccountsWithNegativeBalance(accounts) = " + countAccountsWithNegativeBalance(accounts));

  }

  public static int countEventNumbers(Collection<Integer> integerCollection) {
    int count = 0;

    for (Integer integer : integerCollection) {
      if (integer % 2 == 0) {
        count++;
      }
    }
    return count;
  }

  public static int countOddNumbers(Collection<Integer> integerCollection) {
    int count = 0;

    for (Integer integer : integerCollection) {
      if (integer % 2 != 0) {
        count++;
      }
    }
    return count;
  }

  public static int countPrimeIntegers(Collection<Integer> integerCollection) {
    int count = 0;

    for (Integer integer : integerCollection) {
      if (isPrime(integer)) {
        count++;
      }
    }
    return count;
  }

  public static boolean isPrime(int n) {
    if (n < 2) {
      return false;
    }

    for (int i = 2; i < n; i++) {
      if (n % i == 0) {
        return false;
      }
    }
    return true;
  }

  public static int countStringStartingWithC(Collection<String> stringCollection) {
    int count = 0;

    for (String s : stringCollection) {
      if (s.charAt(0) == 'C') {
        count++;
      }
    }
    return count;
  }

  public static int countAccountsWithNegativeBalance(Collection<Account> accountCollection) {
    int count = 0;

    for (Account account : accountCollection) {
      if (account.getBalance() < 0) {
        count++;
      }
    }
    return count;
  }
}

