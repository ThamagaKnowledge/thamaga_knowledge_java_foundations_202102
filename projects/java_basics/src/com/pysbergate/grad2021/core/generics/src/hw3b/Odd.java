package hw3b;

public class Odd<T> implements GenericCheck<Integer> {

  @Override
  public boolean check(Integer element) {
    return (element != 2);
  }
}
