package hw3b;

public class Account<T> implements GenericCheck<Account> {

  private String accountNum;

  private double balance;

  public Account() {
  }

  public Account(String accountNum, double balance) {
    this.accountNum = accountNum;
    this.balance = balance;
  }

  public double getBalance() {
    return balance;
  }

  @Override
  public boolean check(Account element) {
    if (element.getBalance() < 0) {
      return true;
    }
    return false;
  }
}
