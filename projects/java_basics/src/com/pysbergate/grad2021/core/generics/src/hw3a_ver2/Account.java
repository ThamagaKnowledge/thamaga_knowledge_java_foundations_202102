package hw3a_ver2;

import java.util.ArrayList;
import java.util.List;

public class Account {

  private int accountNum;

  private String name;

  private double balance;

  public Account() {
  }

  private List<Account> negativeBalances = new ArrayList<>();

  public Account(int accountNum, String name, double balance) {
    this.accountNum = accountNum;
    this.name = name;
    this.balance = balance;
  }

  public double getBalance() {
    return balance;
  }

  public List<Account> negativeBalances(List<Account> accounts) {

    for (Account account : accounts) {
      if (account.balance < 0) {
        negativeBalances.add(account);
      }
    }

    for (Account negativeBalance : negativeBalances) {
      System.out.println("negativeBalance = " + negativeBalance);
    }

    return negativeBalances;
  }

  @Override
  public String toString() {
    return
        "accountNum = " + accountNum +
            ", name =" + name + '\'' +
            ", balance = " + balance;
  }
}
