package hw1b;

import codeonthefly.Player;

import java.util.ArrayList;
import java.util.List;

public class Team<T extends Player> {

  private String name;

  private List<T> members = new ArrayList<>();

  public Team(String name) {
    this.name = name;
  }

  public void addPlayer(T player){
    if (members.contains(player)){
      System.out.println("Player already exists");
    }else{
      members.add(player);
    }
  }


}
