package hw1c;

import java.util.ArrayList;
import java.util.List;

public class Fruits {

  public static void main(String[] args) {
    List<String> fruits = new ArrayList<>();
    fruits.add("Orange");
    fruits.add("Peach");
    fruits.add("Pear");
    fruits.add("Banana");
    fruits.add("Apple");
    fruits.add("Mango");

    reverseString(fruits);

  }


  public static List<String> reverseString(List<String> strings) {

    String reverse = "";
    List<String> reversedString = new ArrayList<>();

    for (String string : strings) {

      for (int i = string.length(); i > 0; i--) {
        reverse += string.charAt(i - 1);
      }

      reversedString.add(reverse);
      reverse = "";
    }

    for (String s : reversedString) {
      System.out.println("s = " + s);
    }

    return reversedString;
  }


}
