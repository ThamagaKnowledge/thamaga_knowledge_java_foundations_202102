package hw3a_ver2;

import java.util.ArrayList;
import java.util.List;

public class EvenIntegers {

  public List<Integer> evenInteger(List<Integer> evenIntegers) {
    List<Integer> evenInts = new ArrayList<>();

    for (Integer integer : evenIntegers) {
      if (integer % 2 == 0) {
        evenInts.add(integer);
      }
    }

    for (Integer evenInteger : evenInts) {
      System.out.println("evenInteger = " + evenInteger);
    }
    return evenInts;
  }
}
