package hw3a_ver2;

import java.util.ArrayList;
import java.util.List;

public class StringStartingWithC {

  public List<String> stringStartingWithC(List<String> stringStartingWithC) {
    List<String> strStartingwithC = new ArrayList<>();

    for (String s : stringStartingWithC) {
      if (s.charAt(0) == 'C') {
        strStartingwithC.add(s);
      }
    }

    for (String s : strStartingwithC) {
      System.out.println("s = " + s);
    }

    return strStartingwithC;
  }
}
