package hw3b;

public class Prime<T> implements GenericCheck<Integer> {

  public static boolean isPrime(Integer number) {

    if (number < 2) {
      return false;
    }

    for (int i = 2; i < number; i++) {
      if (number % i == 0) {
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean check(Integer element) {
    if (isPrime(element)) {
      return true;
    }
    return false;
  }
}
