package hw3a_ver2;

import java.util.ArrayList;
import java.util.List;

public class CollectionOfTypes {

  public static void main(String[] args) {
    EvenIntegers evenIntegers = new EvenIntegers();
    OddIntegers oddIntegers = new OddIntegers();
    PrimeIntegers primeIntegers = new PrimeIntegers();
    StringStartingWithC stringStartingWithC = new StringStartingWithC();

    evenIntegers.evenInteger(integerList());
    oddIntegers.oddInteger(integerList());
    primeIntegers.primeInteger(integerList());
    stringStartingWithC.stringStartingWithC(stringList());

    Account account = new Account();

    account.negativeBalances(accounts());

  }

  public static List<Integer> integerList() {
    List<Integer> integers = new ArrayList<>();

    integers.add(2);
    integers.add(4);
    integers.add(1);
    integers.add(7);
    integers.add(3);
    integers.add(67);
    integers.add(244);
    integers.add(21);
    integers.add(354);
    integers.add(100);
    integers.add(5);
    integers.add(11);
    integers.add(34);
    integers.add(17);
    integers.add(23);

    return integers;
  }

  public static List<String> stringList() {
    List<String> strings = new ArrayList<>();
    strings.add("Book");
    strings.add("Cat");
    strings.add("Cold-Drink");
    strings.add("Chisel");

    return strings;
  }

  public static List<Account> accounts(){
    List<Account> accounts = new ArrayList<>();

    Account account = new Account(12,"Pat", -23);
    Account account1 = new Account(13,"Joe", 45);
    Account account2 = new Account(14,"Kat", -45);
    Account account3 = new Account(15,"Kami", -33);
    Account account4 = new Account(16,"Su-Ui", 33);

    accounts.add(account);
    accounts.add(account1);
    accounts.add(account2);
    accounts.add(account3);
    accounts.add(account4);

    return accounts;
  }
}
