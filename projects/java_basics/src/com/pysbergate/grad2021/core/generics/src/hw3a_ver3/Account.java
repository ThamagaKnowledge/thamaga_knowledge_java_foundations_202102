package hw3a_ver3;

public class Account {

  private int accountNum;

  private double balance;

  public Account(int accountNum, double balance) {
    this.accountNum = accountNum;
    this.balance = balance;
  }

  public double getBalance() {
    return balance;
  }
}
