package hw3a_ver2;

import java.util.ArrayList;
import java.util.List;

public class PrimeIntegers {

  public List<Integer> primeInteger(List<Integer> primeIntegers) {
    List<Integer> primes = new ArrayList<>();

    for (Integer primeInteger : primeIntegers) {
      if (isPrime(primeInteger)){
        primes.add(primeInteger);
      }
    }

    for (Integer prime : primes) {
      System.out.println("prime = " + prime);
    }

    return primes;
  }

  private boolean isPrime(int n)
  {
    if (n < 2){
      return false;
    }
    for(int i=2;i<n;i++)
    {
      if(n%i==0)
        return false;
    }
    return true;
  }
}
