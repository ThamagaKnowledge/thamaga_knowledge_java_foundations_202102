package hw3a_ver2;

import java.util.ArrayList;
import java.util.List;

public class OddIntegers {

  public List<Integer> oddInteger(List<Integer> oddIntegers) {
    List<Integer> oddInts = new ArrayList<>();

    for (Integer oddInteger : oddIntegers) {
      if (oddInteger % 2 != 0) {
        oddInts.add(oddInteger);
      }
    }

    for (Integer oddInt : oddInts) {
      System.out.println("oddInteger = " + oddInt);
    }

    return oddInts;
  }
}
