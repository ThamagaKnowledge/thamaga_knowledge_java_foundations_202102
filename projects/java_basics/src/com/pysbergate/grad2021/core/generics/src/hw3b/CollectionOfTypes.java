package hw3b;

import java.util.Arrays;
import java.util.Collection;

public class CollectionOfTypes {

  public static void main(String[] args) {
    Collection<Integer> integerCollection = Arrays.asList(12, 34, 15, 23, 45, 6, 76, 64, 63, 5476, 678, 24, 575, 355, 35);
    Collection<String> stringCollection = Arrays.asList("Chisel", "Book", "Cable", "Candy", "Mouse", "Pen");

    Collection<Account> accountCollection = Arrays.asList(new Account("123", 3434),
        new Account("124", -34434), new Account("125", -3423),
        new Account("126", 3434));

    System.out.println("generalAbstraction(collection, new Even()) = " + generalAbstraction(integerCollection, new Even()));
    System.out.println("generalAbstraction(collection, new Odd()) = " + generalAbstraction(integerCollection, new Odd()));
    System.out.println("generalAbstraction(collection, new Prime()) = " + generalAbstraction(integerCollection, new Prime()));

    System.out
        .println("generalAbstraction(stringCollection,new StringStartingWithC()) = "
            + generalAbstraction(stringCollection, new StringStartingWithC()));

    System.out
        .println("generalAbstraction(accountCollection,new Account()) = "
            + generalAbstraction(accountCollection, new Account()));
  }

  public static <T> int generalAbstraction(Collection<T> collection, GenericCheck<T> genericCheck) {
    int count = 0;

    for (T element : collection) {
      if (genericCheck.check(element)) {
        count++;
      }

    }
    return count;
  }
}
