package hw3b;

public interface GenericCheck<T> {

  boolean check(T element);
}
