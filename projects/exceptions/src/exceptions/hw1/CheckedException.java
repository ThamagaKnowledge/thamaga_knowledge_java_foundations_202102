package exceptions.hw1;

public class CheckedException {

  public static void main(String[] args) throws Exception {
    method01();
  }

  public static void method01() throws Exception {
    method02();
  }

  public static void method02() throws Exception {
    method03();
  }

  public static void method03() throws Exception {
    method04();
  }

  public static void method04() throws Exception {
    method05();
  }

  public static void method05() throws Exception {
    throw new Exception();
  }

}
