package exceptions.ce1a;

import java.util.Scanner;

public class SquashException {


  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.print("Enter your Age: ");
    int age = scanner.nextInt();

    try {
      validateAge(age);
    } catch (UnderAgeException e) {
      //e.printStackTrace();

      //the exception has been squashed. meaning it will not do anything.
    }
    System.out.println("Outside try-Catch: ");

  }

  public static void validateAge(int age) throws UnderAgeException {
    if (age <= 21) {
      throw new UnderAgeException("You are under Age, you do not qualify to make a License", age);
    }

  }
}

