package com.psybergate.grad2021.core.exceptions.ce2a;

public class ClassB extends Throwable {

  public static void main(String[] args) {

    try {
      printCustomerName();
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      printCustomerAge();
    } catch (Throwable throwable) {
      throwable.printStackTrace();
    }

    printCustomerEmailAddress();
    printCustomerAddress();

    System.out.println("End of the Program");
  }


//Methods to print some Customer Attributes.

  //Propagates a Checked Exception
  public static void printCustomerName() throws Exception {
    throw new Exception("Name: some Customer Name.");
  }

  //Propagates a Throwable (which is a Checked Exception)
  public static void printCustomerAge() throws Throwable {
    throw new Throwable("Age: Some Customer Age");
  }

  //Propagates a Runtime Exception
  public static void printCustomerAddress() {
    throw new RuntimeException("Address: Some Customer Address.");
  }

  //Propagates an Error (which is a type of Runtime Exception)
  public static void printCustomerEmailAddress() throws Error {
    throw new Error("EmailAddress: Some Customer Email Address.");
  }

}
