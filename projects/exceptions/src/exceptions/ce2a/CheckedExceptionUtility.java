package com.psybergate.grad2021.core.exceptions.ce2a;

public class CheckedExceptionUtility {

  public static void main(String[] args) throws CheckedException {
    checkedException();

    //you can either handle the exception in this method by doing
    // the try-catch or throw the exception (propagate it), this time
    // to the JVM.
  }

  public static void checkedException() throws CheckedException {
    throw new CheckedException("This is a Checked Exception: ");

    //The code won't compile if an exception is not explicitly
    //propagated
  }
}
