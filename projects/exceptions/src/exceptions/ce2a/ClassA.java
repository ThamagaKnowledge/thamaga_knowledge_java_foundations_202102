package com.psybergate.grad2021.core.exceptions.ce2a;

public class ClassA {

  public static void main(String[] args) throws Throwable {

    callPrintCustomerAge();
    callPrintCustomerName();

    callPrintCustomerAddress();
    callPrintCustomerEmailAddress();

  }

  public static void callPrintCustomerName() throws Exception {
    ClassB.printCustomerName();
  }

  public static void callPrintCustomerAge() throws Throwable {
    ClassB.printCustomerAge();
  }

  public static void callPrintCustomerAddress() {
    ClassB.printCustomerAddress();
  }

  public static void callPrintCustomerEmailAddress() {
    ClassB.printCustomerEmailAddress();
  }
}
