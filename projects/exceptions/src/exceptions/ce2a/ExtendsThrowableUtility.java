package com.psybergate.grad2021.core.exceptions.ce2a;

public class ExtendsThrowableUtility extends Throwable{

  public static void main(String[] args) throws ThrowableException {
    extendsThrowable();
  }

  public static void extendsThrowable() throws ThrowableException {
   throw new ThrowableException("This Exception extends Throwable: ");

    //Handle the Error because Throwable is a Checked Exception.

  }
}
