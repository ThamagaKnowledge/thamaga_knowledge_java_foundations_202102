package com.psybergate.grad2021.core.exceptions.ce2a;

public class RuntimeExceptionUtility {

  public static void main(String[] args) {
    runTimeException();

    //Do not have to Explicitly Propagate it.
  }

  public static void runTimeException(){
    throw new RuntimeException("This is a Runtime Exception: ");

    //The Code compiles because a Runtime exception is AUTOMATICALLY
    // Propagated by the Compiler. ???? (Is it really Propagated by the Compiler ?)
    //OR the compiler only understands that it is AUTOMATICALLY Propagated, therefore
    //does not complain ?
  }
}

