package com.psybergate.grad2021.core.interfaces.chrisversion.standards;

public class DefaultCommandResponse implements CommandResponse{

  private String response;

  public DefaultCommandResponse(String response) {
    this.response = response;
  }

  @Override
  public String response() {
    return this.response;
  }
}
