package com.psybergate.grad2021.core.interfaces.ce3_ver2.vendorimpl1;

import com.psybergate.grad2021.core.interfaces.ce3.standards.Commands;

import java.time.LocalDate;
import java.util.Date;

/*The Client is responsible for implementing the Command*/

public class CommandsImpl1 implements Commands {

  @Override
  public double sum(double num1, double num2) {
//    System.out.println("sum = " + (num1 + num2));
    return (num1 + num2);
  }

  @Override
  public int factorial(int num) {
    int factorial = 1;

    do {
      factorial = factorial * num;
      num--;
    } while (num > 0);
    return factorial;
  }

  @Override
  public LocalDate getCurrentDate() {
    return LocalDate.now();
  }
}
