package com.psybergate.grad2021.core.interfaces.ce1.standards;

public class InvalidDateException extends Exception{

  public InvalidDateException(String message) {
    super(message);
  }
}
