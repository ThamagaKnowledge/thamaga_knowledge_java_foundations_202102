package com.psybergate.grad2021.core.interfaces.ce3_ver2.client;

import com.psybergate.grad2021.core.interfaces.ce3_ver2.standards.Command;
import com.psybergate.grad2021.core.interfaces.ce3_ver2.vendorimpl1.CommandsImpl1;

public class CommandImpl implements Command {

  @Override
  public void setCommand(Object command) {
    command = new CommandsImpl1();
  }
}
