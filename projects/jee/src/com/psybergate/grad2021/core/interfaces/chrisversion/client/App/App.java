package com.psybergate.grad2021.core.interfaces.chrisversion.client.App;

import com.psybergate.grad2021.core.interfaces.chrisversion.client.command.SumNumbers;
import com.psybergate.grad2021.core.interfaces.chrisversion.standards.CommandEngine;
import com.psybergate.grad2021.core.interfaces.chrisversion.standards.CommandRequest;
import com.psybergate.grad2021.core.interfaces.chrisversion.standards.CommandResponse;
import com.psybergate.grad2021.core.interfaces.chrisversion.standards.DefaultCommandRequest;
import com.psybergate.grad2021.core.interfaces.chrisversion.vendorimpl.CommandEngineChris;

import java.util.Arrays;
import java.util.List;

public class App {

  public static void main(String[] args) {
    CommandRequest commandRequest = new DefaultCommandRequest(new SumNumbers());
    List<Integer> integers = Arrays.asList(1,2,3);
    commandRequest.setParameters("numbers", integers);

    CommandEngine commandEngine = new CommandEngineChris();
    CommandResponse commandResponse = commandEngine.processCommand(commandRequest);
    System.out.println(commandResponse.response());
  }
}
