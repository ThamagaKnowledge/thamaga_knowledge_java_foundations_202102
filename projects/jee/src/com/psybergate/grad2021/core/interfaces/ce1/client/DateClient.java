package com.psybergate.grad2021.core.interfaces.ce1.client;

import com.psybergate.grad2021.core.interfaces.ce1.standards.Date;
import com.psybergate.grad2021.core.interfaces.ce1.standards.DateFactory;
import com.psybergate.grad2021.core.interfaces.ce1.standards.DateFactoryLoader;
import com.psybergate.grad2021.core.interfaces.ce1.standards.InvalidDateException;

public class DateClient {

  public static void main(String[] args) throws InvalidDateException {
    getDate();
  }

  public static void getDate()
      throws InvalidDateException {

    DateFactory dateFactory = new DateFactoryLoader().getDateFactory();
    Date date = dateFactory.createDate(1, 3, 2020);

    System.out.println(date.getDay());
    System.out.println("DateFactory = " + dateFactory.getClass());
    System.out.println("Date = " + date);
  }

}
