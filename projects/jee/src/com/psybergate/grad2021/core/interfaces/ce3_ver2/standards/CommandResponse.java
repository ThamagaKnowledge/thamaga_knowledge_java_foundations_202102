package com.psybergate.grad2021.core.interfaces.ce3_ver2.standards;

public interface CommandResponse {

  Object result();
}
