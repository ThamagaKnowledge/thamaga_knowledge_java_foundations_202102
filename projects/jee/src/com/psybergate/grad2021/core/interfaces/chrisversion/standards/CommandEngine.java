package com.psybergate.grad2021.core.interfaces.chrisversion.standards;

import com.psybergate.grad2021.core.interfaces.chrisversion.standards.CommandResponse;

public interface CommandEngine {
  CommandResponse processCommand(CommandRequest request);
}
