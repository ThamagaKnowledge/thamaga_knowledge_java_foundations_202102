package com.psybergate.grad2021.core.interfaces.chrisversion.vendorimpl;

import com.psybergate.grad2021.core.interfaces.chrisversion.standards.CommandResponse;
import com.psybergate.grad2021.core.interfaces.chrisversion.standards.Command;
import com.psybergate.grad2021.core.interfaces.chrisversion.standards.CommandEngine;
import com.psybergate.grad2021.core.interfaces.chrisversion.standards.CommandRequest;

public class CommandEngineChris implements CommandEngine {

  @Override
  public CommandResponse processCommand(CommandRequest request) {
    Command command = request.getCommand();
    CommandResponse res = command.execute(request);

    return res;
  }
}
