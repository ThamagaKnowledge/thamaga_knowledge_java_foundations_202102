package com.psybergate.grad2021.core.interfaces.ce3_ver2.vendorimpl1;

import com.psybergate.grad2021.core.interfaces.ce3_ver2.standards.CommandEngine;
import com.psybergate.grad2021.core.interfaces.ce3_ver2.standards.CommandResponse;

public class CommandEngineImpl1 implements CommandEngine {

  @Override
  public void execute(CommandResponse command) {
    System.out.println(command.result());
  }
}
