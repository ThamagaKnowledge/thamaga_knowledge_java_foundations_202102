package com.psybergate.grad2021.core.interfaces.ce3_ver2.client;

import com.psybergate.grad2021.core.interfaces.ce3_ver2.standards.Command;
import com.psybergate.grad2021.core.interfaces.ce3_ver2.standards.CommandEngine;
import com.psybergate.grad2021.core.interfaces.ce3_ver2.standards.CommandResponse;
import com.psybergate.grad2021.core.interfaces.ce3_ver2.standards.Commands;
import com.psybergate.grad2021.core.interfaces.ce3_ver2.vendorimpl1.CommandEngineImpl1;
import com.psybergate.grad2021.core.interfaces.ce3_ver2.vendorimpl1.CommandResponseImpl1;
import com.psybergate.grad2021.core.interfaces.ce3_ver2.vendorimpl1.CommandsImpl1;

public class CommandEngineClient {

  public static void main(String[] args) {

    CommandEngine commandEngine = new CommandEngineImpl1();

   CommandsImpl1 commandsImpl1 = new CommandsImpl1();

    CommandResponse cr = new CommandResponseImpl1(commandsImpl1);

    commandEngine.execute(cr);
  }
}
