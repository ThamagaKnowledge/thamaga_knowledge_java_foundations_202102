package com.psybergate.grad2021.core.interfaces.ce1.vendorimpl1;

import com.psybergate.grad2021.core.interfaces.ce1.standards.Date;
import com.psybergate.grad2021.core.interfaces.ce1.standards.DateFactory;
import com.psybergate.grad2021.core.interfaces.ce1.standards.InvalidDateException;

public class DateFactoryImpl implements DateFactory {

  @Override
  public Date createDate(int day, int month, int year)
      throws InvalidDateException {
    return new MyDateImpl(day, month, year);
  }

}
