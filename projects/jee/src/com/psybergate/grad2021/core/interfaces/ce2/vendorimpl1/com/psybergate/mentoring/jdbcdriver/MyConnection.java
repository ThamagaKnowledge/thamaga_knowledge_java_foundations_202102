package com.psybergate.grad2021.core.interfaces.ce2.vendorimpl1.com.psybergate.mentoring.jdbcdriver;

import com.psybergate.grad2021.core.interfaces.ce2.vendorimpl1.Connection;
import com.psybergate.grad2021.core.interfaces.ce2.vendorimpl1.Statement;

public class MyConnection implements Connection {

  @Override
  public Connection getConnection(String url, String user, String pw) {
    return null;
  }

  @Override
  public Statement createStatement() {
    return null;
  }
}
