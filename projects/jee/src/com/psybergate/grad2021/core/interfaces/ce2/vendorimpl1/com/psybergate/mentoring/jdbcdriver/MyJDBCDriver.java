package com.psybergate.grad2021.core.interfaces.ce2.vendorimpl1.com.psybergate.mentoring.jdbcdriver;

import com.psybergate.grad2021.core.interfaces.ce2.vendorimpl1.JDBCDriver;
import com.psybergate.grad2021.core.interfaces.ce2.vendorimpl1.ResultSet;
import com.psybergate.grad2021.core.interfaces.ce2.vendorimpl1.Statement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class MyJDBCDriver implements JDBCDriver {

  @Override
  public void registerDriver() {

  }

  @Override
  public Connection connect(String url, Properties info) {
    return null;
  }

  @Override
  public boolean acceptsURL(String url) {
    return false;
  }

  @Override
  public int getMajorVersion() {
    return 0;
  }
}
