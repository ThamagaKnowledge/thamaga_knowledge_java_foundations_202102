package com.psybergate.grad2021.core.interfaces.ce1.vendorimpl1;

import com.psybergate.grad2021.core.interfaces.ce1.standards.Date;
import com.psybergate.grad2021.core.interfaces.ce1.standards.InvalidDateException;

public class MyDateImpl implements Date {

  private int day;

  private int month;

  private int year;

  public MyDateImpl(int day, int month, int year) throws InvalidDateException {
    validateDate(day, month, year);
    this.day = day;
    this.month = month;
    this.year = year;
  }

  public void validateDate(int day, int month, int year) throws InvalidDateException {
    if (day <= 0 || day > 31) {
      throw new InvalidDateException("Day is Invalid");
    }
    if (month <= 0 || month > 12) {
      throw new InvalidDateException("Month is Invalid");
    }

    if (month == 4 || month == 6 || month == 9 || month == 11) {
      if (day > 30) {
        throw new InvalidDateException("Month is Invalid");
      }
    }

    if (year <= -2000 || year >= 2099) {
      throw new InvalidDateException("Month is Invalid");
    }

    if (isLeapYear()) {
      throw new InvalidDateException("Year is Invalid - Leap year");
    }
  }

  @Override
  public int getDay() {
    return this.day;
  }

  @Override
  public int getMonth() {
    return this.month;
  }

  @Override
  public int getYear() {
    return this.year;
  }

  @Override
  public boolean isLeapYear() {
    //if the year is divisible by 4
    //For a non-century year

    boolean leap = false;

    // if the year is divided by 4
    if (year % 4 == 0) {

      // if the year is century
      if (year % 100 == 0) {

        // if year is divided by 400
        // then it is a leap year
        if (year % 400 == 0)
          leap = true;
        else
          leap = false;
      }

      // if the year is not century
      else
        leap = true;
    }

    else
      leap = false;

    return leap;
  }

  @Override
  public Date addDays(int days) {
    return null;
  }
}
