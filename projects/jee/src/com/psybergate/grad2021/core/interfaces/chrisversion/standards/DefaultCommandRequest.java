package com.psybergate.grad2021.core.interfaces.chrisversion.standards;

import java.util.HashMap;
import java.util.Map;

public class DefaultCommandRequest implements CommandRequest{

  private Command command;

  private Map<String, Object> parameters = new HashMap<>();

  public DefaultCommandRequest(Command command) {
    this.command = command;
  }

  public Command getCommand() {
    return command;
  }

  public void setParameters(String name, Object parameterValue) {
    parameters.put(name, parameterValue);
  }

  public Object getParameters(String name) {
    return parameters.get(name);
  }

}
