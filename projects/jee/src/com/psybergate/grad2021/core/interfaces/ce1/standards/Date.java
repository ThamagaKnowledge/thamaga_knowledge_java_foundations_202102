package com.psybergate.grad2021.core.interfaces.ce1.standards;

public interface Date {

  int getDay();

  int getMonth();

  int getYear();

  boolean isLeapYear();

  Date addDays(int days);
}
