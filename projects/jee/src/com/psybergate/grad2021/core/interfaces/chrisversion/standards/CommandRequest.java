package com.psybergate.grad2021.core.interfaces.chrisversion.standards;

public interface CommandRequest {

  public void setParameters(String name, Object parameterValue);

  public Object getParameters(String name);

  Command getCommand();

}
