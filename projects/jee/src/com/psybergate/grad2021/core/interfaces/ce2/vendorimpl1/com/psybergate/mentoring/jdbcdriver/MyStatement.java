package com.psybergate.grad2021.core.interfaces.ce2.vendorimpl1.com.psybergate.mentoring.jdbcdriver;

import com.psybergate.grad2021.core.interfaces.ce2.vendorimpl1.ResultSet;
import com.psybergate.grad2021.core.interfaces.ce2.vendorimpl1.Statement;

public class MyStatement implements Statement {
  @Override
  public int executeUpdate() {
    return 0;
  }

  @Override
  public ResultSet executeQuery(String select_from_mytable) {
    return null;
  }

  @Override
  public void close() {

  }
}
