package com.psybergate.grad2021.core.interfaces.ce3_ver2.standards;

import java.util.Date;

public interface Commands {
  /*Could be improved to Sum more numbers - Through a List*/
  double sum(double num1, double num2);

  int factorial(int num);

  Date getCurrentDate();
}
