package com.psybergate.grad2021.core.interfaces.ce2.vendorimpl1;

import java.sql.Connection;
import java.util.Properties;

public interface JDBCDriver {

  void registerDriver();

  Connection connect(String url, Properties info);

  boolean acceptsURL(String url);

  int getMajorVersion();
}
