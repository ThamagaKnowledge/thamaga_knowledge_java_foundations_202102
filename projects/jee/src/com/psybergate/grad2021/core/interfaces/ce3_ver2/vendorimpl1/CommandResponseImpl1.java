package com.psybergate.grad2021.core.interfaces.ce3_ver2.vendorimpl1;

import com.psybergate.grad2021.core.interfaces.ce3_ver2.standards.CommandResponse;

public class CommandResponseImpl1 implements CommandResponse {

  private CommandsImpl1 commandsImpl1;

  public CommandResponseImpl1(CommandsImpl1 commandsImpl1) {
    this.commandsImpl1 = commandsImpl1;
  }

  @Override
  public Object result() {
//    return commandsImpl1.factorial(5);
//    return commandsImpl1.sum(5,5);
    return commandsImpl1.getCurrentDate();
  }
}
