package com.psybergate.grad2021.core.interfaces.ce2.client;

import com.psybergate.grad2021.core.interfaces.ce2.vendorimpl1.Connection;
import com.psybergate.grad2021.core.interfaces.ce2.vendorimpl1.ResultSet;
import com.psybergate.grad2021.core.interfaces.ce2.vendorimpl1.Statement;

import java.sql.DriverManager;

public class JDBCDriverClient {

  public static void main(String[] args) {

    try {
      Class.forName("com.psybergate.mydatabase.jdbcdriver.MyJDBCDriver");
      Connection connection = (Connection) DriverManager.getConnection("","","");
      Statement statement = connection.createStatement();
      ResultSet rs = statement.executeQuery("select from mytable");
    }catch (Exception e){
      e.printStackTrace();
    }
  }
}
