package com.psybergate.grad2021.core.interfaces.ce2.vendorimpl1;

public interface Connection {

  /**
   *
   * @return : Establish and returns a connection object
   */
  Connection getConnection(String url, String user, String pw);

  /**
   *
   * @return : returns a statement object to execute SQL statements.
   */
  Statement createStatement();
}
