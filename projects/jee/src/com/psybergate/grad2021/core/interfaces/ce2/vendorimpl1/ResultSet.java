package com.psybergate.grad2021.core.interfaces.ce2.vendorimpl1;

public interface ResultSet {

  /**
   * Moves the cursor to the first row in this ResultSet object
   */
  void first();

  /**
   * Moves the cursor to the last row in this ResultSet object
   */
  void last();

  /**
   *
   * @return : returns the value as int of a given comment
   */
  int getInt();

  /**
   *
   * @return : returns the value as String of a given comment
   */
  String getString();

  /**
   *
   * @return : returns the true if the column fetched by the last get(*)
   * method has a null value
   */
  boolean wasNull();

  /**
   * Closes the Resources used by this ResultSet Object
   */
  void close();
}
