package com.psybergate.grad2021.core.interfaces.ce2.vendorimpl1;

public interface Statement {

  /**
   *
   * @return : Method returns the number of rows affected by the
   * SQL command it executes
   */
  int executeUpdate();



  /**
   *
   * @return : Returns one ResultSet Object
   * @param select_from_mytable
   */
  ResultSet executeQuery(String select_from_mytable);

  /**
   *
   */
  void close();
}
