package com.psybergate.grad2021.core.interfaces.ce1.standards;

import java.io.InputStream;
import java.util.Properties;

public class DateFactoryLoader {

  private static DateFactory dateFactory;

  public DateFactory getDateFactory() {
    if (dateFactory != null) {
      return dateFactory;
    }
    try (InputStream is = DateFactoryLoader.class.getClassLoader().getResourceAsStream("date.properties")) {
      System.out.println(is);
      Properties properties = new Properties();
      properties.load(is);
      String dateFactoryClass = properties.getProperty("date.factory");
      dateFactory =
          (DateFactory) Class.forName(dateFactoryClass).getDeclaredConstructor(int.class, int.class, int.class)
              .newInstance(1, 4, 2020); //clazz.getDeclaredConstructor().newInstance()
      return dateFactory;
    } catch (Exception e) {
      throw new RuntimeException("Error - loading date factory class dynamically");
    }
  }

}
