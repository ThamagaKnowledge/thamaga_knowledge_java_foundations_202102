package com.psybergate.grad2021.core.interfaces.chrisversion.standards;

public interface CommandResponse {

  String response();
}
