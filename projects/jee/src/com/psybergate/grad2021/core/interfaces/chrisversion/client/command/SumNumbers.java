package com.psybergate.grad2021.core.interfaces.chrisversion.client.command;

import com.psybergate.grad2021.core.interfaces.chrisversion.standards.Command;
import com.psybergate.grad2021.core.interfaces.chrisversion.standards.CommandRequest;
import com.psybergate.grad2021.core.interfaces.chrisversion.standards.CommandResponse;
import com.psybergate.grad2021.core.interfaces.chrisversion.standards.DefaultCommandResponse;

import java.util.List;

public class SumNumbers implements Command {
  @Override
  public CommandResponse execute(CommandRequest request) {
    List<Integer> numbers = (List<Integer>) request.getParameters("numbers");
int result = 0;
    for (Integer number : numbers) {
      result += number;
    }
    return new DefaultCommandResponse(result + "");
  }
}
