package com.psybergate.grad2021.core.interfaces.ce2.vendorimpl1.com.psybergate.mentoring.jdbcdriver;

import com.psybergate.grad2021.core.interfaces.ce2.vendorimpl1.ResultSet;

public class MyResultSet implements ResultSet {
  @Override
  public void first() {

  }

  @Override
  public void last() {

  }

  @Override
  public int getInt() {
    return 0;
  }

  @Override
  public String getString() {
    return null;
  }

  @Override
  public boolean wasNull() {
    return false;
  }

  @Override
  public void close() {

  }
}
