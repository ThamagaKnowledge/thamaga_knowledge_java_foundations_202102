package com.psybergate.grad2021.core.oobasics_part1.ce4a;

public class CurrentAccountUtils {

  public static void main(String[] args) {

    Account account = new Account(2, "Account", 0, "Account");
    CurrentAccount currentAccount = new CurrentAccount();
    CurrentAccount currentAccount1 = new CurrentAccount(1, "Knolwedge", 4_000, "SavingsAccount", 0.0);

    System.out.println("currentAccount.printAccount() = " + currentAccount.toString());
    System.out.println("currentAccount1.printAccount() = " + currentAccount1.toString());
    System.out.println("account.printAccount() = " + account.toString());
  }
}
