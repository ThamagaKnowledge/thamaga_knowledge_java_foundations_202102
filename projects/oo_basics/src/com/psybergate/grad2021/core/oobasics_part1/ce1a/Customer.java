package com.psybergate.grad2021.core.oobasics_part1.ce1a;

import java.util.ArrayList;
import java.util.List;

public class Customer {
    private int customerNum;

    private String name;

    private String address;

    public Customer(int customerNum, String name, String address) {
        this.customerNum = customerNum;
        this.name = name;
        this.address = address;
    }

    public List<Double> addCurrentAccount(double accountNum, double balance) {
        List<Double> list = new ArrayList<>();

        list.add(accountNum);
        list.add(balance);

        return list;
    }

}

