package com.psybergate.grad2021.core.oobasics_part1.demo1a;

public class Bike extends Vehicle {

  @Override
  void bike() {
    System.out.println("Bike is running:");
  }

  public static void main(String[] args) {
    Bike bike = new Bike();
    bike.bike();
  }
}
