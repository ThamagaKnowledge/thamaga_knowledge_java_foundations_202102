package com.psybergate.grad2021.core.oobasics_part1.ce4a;

public class CurrentAccount extends Account {

  private double interest;

  //Usage of “this” in terms of one constructor calling another constructor
  public CurrentAccount() {
    this(1, "Thamaga", 5_000, "Current Account", 0.02);
  }

  //Usage of “this” and “super” in constructors in terms of usage of parent constructors, and
  //initialisation of variables

  public CurrentAccount(int accountNumber, String accountHolderName, double balance, String accountType, double interest) {
    super(accountNumber, accountHolderName, balance, accountType);
    this.interest = interest;
  }

  @Override
  public double deposit(int amount) {
    return super.deposit(amount);
  }

  //Usage of “this” in a normal method
  public void displayAmountAfterDeposit(int amount) {
    System.out.println("Method calling another method using this" +
        this.deposit(amount));
  }

  //Create a toString() method in A, and write a method in B that tries to call the toString()
  //method in class java.lang.Object
  public void methodToCallToString() {
    toString();
  }
}
