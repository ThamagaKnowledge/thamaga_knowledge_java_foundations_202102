package com.psybergate.grad2021.core.oobasics_part1.ce4a;

public class Account {

  private int accountNumber;

  private String accountHolderName;

  private double balance;

  private String accountType;

  public Account(int accountNumber, String accountHolderName, double balance, String accountType) {
    this.accountNumber = accountNumber;
    this.accountHolderName = accountHolderName;
    this.balance = balance;
    this.accountType = accountType;
  }

  public double deposit(int amount) {
    return this.balance += amount;
  }

  @Override
  public String toString() {
    return "Account{" +
        "accountNumber=" + accountNumber +
        ", accountHolderName='" + accountHolderName + '\'' +
        ", balance=" + balance +
        ", accountType='" + accountType + '\'' +
        '}';
  }
}
