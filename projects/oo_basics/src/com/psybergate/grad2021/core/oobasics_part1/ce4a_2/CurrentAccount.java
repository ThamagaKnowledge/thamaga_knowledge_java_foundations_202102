package com.psybergate.grad2021.core.oobasics_part1.ce4a_2;

public class CurrentAccount extends Account {

private double interestRate;

  public CurrentAccount(int accountNum, String accountHolderName, String accountType, double balance, double interestRate) {
    super(accountNum, accountHolderName, accountType, balance);
    this.interestRate = interestRate;
  }

  @Override
  public boolean isOverdrawn() {
    return super.isOverdrawn();
  }

  @Override
  public boolean needsToBeReviewed() {
    return super.needsToBeReviewed();
  }
}
