package com.psybergate.grad2021.core.oobasics_part1.hw5a;

import java.util.ArrayList;
import java.util.List;

public class Customer {

  private final String customerNumber;

  private final String customerName;

  private List<Account> accounts = new ArrayList<>();

  public Customer() {
    this.customerNumber = "1";
    this.customerName = "Thamaga";
  }

  Account account1 = new CurrentAccount(1, 2000, 10_000);
  Account account2 = new CurrentAccount(2, 1000, 10_000);
  Account account3 = new CurrentAccount(3, 3000, 10_000);

  Account account4 = new SavingsAccount(1,3000,5000);
  Account account5 = new SavingsAccount(2, 2000, 5000);
  Account account6 = new SavingsAccount(3, 7000, 5000);

  public List<Account> getAccounts() {
    accounts.add(account1);
    accounts.add(account2);
    accounts.add(account3);
    accounts.add(account4);
    accounts.add(account5);
    accounts.add(account6);

    return accounts;
  }



  public String printCustomer() {
    return "Customer{" +
        "customerNumber='" + customerNumber + '\'' +
        ", customerName='" + customerName + '\'' +
        '}';
  }
}
