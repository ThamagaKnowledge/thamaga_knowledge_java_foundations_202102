package com.psybergate.grad2021.core.oobasics_part1.ce4a_2;

public class Account {

  private int accountNum;

  private String accountHolderName;

  private String accountType;

  private double balance;

  public Account(int accountNum, String accountHolderName, String accountType, double balance) {
    this.accountNum = accountNum;
    this.accountHolderName = accountHolderName;
    this.accountType = accountType;
    if (accountType.equals("Savings Account") && (balance < 0)){
      throw new IllegalArgumentException();
    }

    this.balance = balance;

  }


  public double getBalance() {
    return balance;
  }

  public boolean isOverdrawn() {
    return getBalance() < 0;
  }

  public boolean needsToBeReviewed() {
    if (isOverdrawn() && (getBalance() <= (-1 * 50_000))) {
      System.out.println("Account is overdrawn and needs to be reviewed");
      return true;
    }
    System.out.println("Account is overdrawn but need not be reviewed");
    return false;
  }

  public void withdraw() {

  }

  public void deposit() {

  }

}
