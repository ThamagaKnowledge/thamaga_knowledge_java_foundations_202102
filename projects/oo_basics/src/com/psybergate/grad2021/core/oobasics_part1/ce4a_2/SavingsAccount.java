package com.psybergate.grad2021.core.oobasics_part1.ce4a_2;

public class SavingsAccount extends Account {

  public SavingsAccount(int accountNum, String accountHolderName, String accountType, double balance) {
    super(accountNum, accountHolderName, accountType, balance);
  }

  @Override
  public boolean isOverdrawn() {
    return getBalance() < 5_000;
  }

  @Override
  public boolean needsToBeReviewed() {
    if (getBalance() < 2_000) {
      System.out.println("Account needs to be reviewed");
      return true;
    }
    System.out.println("Account needs not to be reviewed");
    return false;
  }
}
