package com.psybergate.grad2021.core.oobasics_part1.ce1a;

import java.util.ArrayList;

public class CustomerUtils {

    Customer customer = new Customer(1, "Thamaga", "50031 River-Side A");

    public void printSum() {
        ArrayList arrayList = new ArrayList();

        arrayList.add(customer.addCurrentAccount(1234, 50.00).get(1));
        arrayList.add(customer.addCurrentAccount(1234, 100.00).get(1));
        arrayList.add(customer.addCurrentAccount(1234, 200.00).get(1));
        arrayList.add(customer.addCurrentAccount(1234, 200.00).get(1));

        double totalBalance = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            totalBalance = totalBalance + (double) arrayList.get(i);
        }

        System.out.println("totalBalance = " + totalBalance);

    }

    public static void main(String[] args) {
        CustomerUtils customerUtils = new CustomerUtils();
        customerUtils.printSum();

    }

}

