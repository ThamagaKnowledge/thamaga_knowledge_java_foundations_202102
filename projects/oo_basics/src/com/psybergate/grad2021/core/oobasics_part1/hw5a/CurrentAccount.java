package com.psybergate.grad2021.core.oobasics_part1.hw5a;

public class CurrentAccount extends Account{

  private double overdraft;

  public CurrentAccount(int accountNumber, double balance, double overdraft) {
    super(accountNumber, balance);
    this.overdraft = overdraft;
  }
}
