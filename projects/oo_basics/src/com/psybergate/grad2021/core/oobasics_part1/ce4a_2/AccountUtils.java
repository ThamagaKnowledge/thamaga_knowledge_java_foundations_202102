package com.psybergate.grad2021.core.oobasics_part1.ce4a_2;

public class AccountUtils {

  public static void main(String[] args) {

    CurrentAccount currentAccount = new CurrentAccount(1, "Thamaga", "Current Account",50_000 , 0.06);
    CurrentAccount currentAccount1 = new CurrentAccount(2, "Knowledge", "Current Account", -60_000, 0.08);

    SavingsAccount savingsAccount = new SavingsAccount(1, "Su", "Savings Account", 7000);
    SavingsAccount savingsAccount1 = new SavingsAccount(2, "Yen", "Savings Account", 1000);

    System.out.println("currentAccount.needsToBeReviewed() = " + currentAccount.needsToBeReviewed());
    System.out.println("currentAccount1.needsToBeReviewed() = " + currentAccount1.needsToBeReviewed());

    System.out.println("savingsAccount.needsToBeReviewed() = " + savingsAccount.needsToBeReviewed());
    System.out.println("savingsAccount1.needsToBeReviewed() = " + savingsAccount1.needsToBeReviewed());
  }
}
