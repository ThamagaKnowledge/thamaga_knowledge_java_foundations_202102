package com.psybergate.grad2021.core.oobasics_part1.ce2a;

public class Account {

  private static final int MAX_OVERDRAFT = 10_000;

  private int accountNum;

  private String accountHolderName;

  private double balance;

  private String accountType;

  public Account(int accountNum, String accountHolderName, double balance, String accountType) {
    this.accountNum = accountNum;
    this.accountHolderName = accountHolderName;
    this.balance = balance;
    this.accountType = accountType;
  }

  public double getBalance() {
    return balance;
  }

  public boolean isOverdrawn() {
   return getBalance() < 0;
  }

  public boolean needsToBeReviewed() {
    if (isOverdrawn() && (getBalance() <= (-1 * 50_000))) {
      System.out.println("Account is overdrawn and needs to be reviewed");
      return true;
    }
    System.out.println("Account is overdrawn but need not be reviewed");
    return false;
  }

  public void withdraw() {

  }

  public void deposit() {

  }

}
