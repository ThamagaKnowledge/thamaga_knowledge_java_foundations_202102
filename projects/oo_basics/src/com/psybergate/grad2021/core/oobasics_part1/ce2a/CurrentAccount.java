package com.psybergate.grad2021.core.oobasics_part1.ce2a;

public class CurrentAccount extends Account{

private double interestRate;

  public CurrentAccount(int accountNum, String accountHolderName, double balance, String accountType, double interestRate) {
    super(accountNum, accountHolderName, balance, accountType);
    this.interestRate = interestRate;
  }

  @Override
  public boolean isOverdrawn() {
    return super.isOverdrawn();
  }

  @Override
  public boolean needsToBeReviewed() {
    return super.needsToBeReviewed();
  }
}
