package com.psybergate.grad2021.core.oobasics_part1.hw5a;

public class SavingsAccount extends Account{

  private double minBalance;

  public SavingsAccount(int accountNumber, double balance, double minBalance) {
    super(accountNumber, balance);
    this.minBalance = minBalance;
  }
}
