package com.psybergate.grad2021.core.oobasics_part1.ce3a;

import java.util.ArrayList;
import java.util.List;

public class CurrentAccountUtils {

  public static void main(String[] args) {

    ArrayList<CurrentAccount> accountArrayList = new ArrayList<>();

    //Object is guaranteed to Implement functionality defined by the Type of Object Reference.
    CurrentAccount currentAccount = new CurrentAccount(1,
        "Thamaga", 40000, "Current Acc",
        0.9);

    CurrentAccount currentAccount2 = new CurrentAccount(1,
        "Thamaga", -70000, "Current Acc",
        0.9);

    CurrentAccount currentAccount3 = new CurrentAccount(1,
        "Thamaga", -50000, "Current Acc",
        0.9);

    accountArrayList.add(currentAccount);
    accountArrayList.add(currentAccount2);
    accountArrayList.add(currentAccount3);

    System.out.println(currentAccount.print());
    System.out.println(currentAccount2.print());
    System.out.println(currentAccount3.print());

    //Getting an Object from an ArrayList and mutating it, putting
    // it back into the Arraylist will change the state you have
    //Mutated.
    accountArrayList.get(1).setAccountHolderName("Knowledge");

    accountArrayList.add(currentAccount2);

    System.out.println(currentAccount2.print());

  }
}
