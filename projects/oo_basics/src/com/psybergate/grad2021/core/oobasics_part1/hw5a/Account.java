package com.psybergate.grad2021.core.oobasics_part1.hw5a;

public class Account {

  private int accountNumber;

  private double balance;

  public Account(int accountNumber, double balance) {
    this.accountNumber = accountNumber;
    this.balance = balance;
  }


  public String printAccount() {
    return "Account{" +
        "customer="  +
        ", accountNumber=" + accountNumber +
        ", balance=" + balance +
        '}';
  }
}
