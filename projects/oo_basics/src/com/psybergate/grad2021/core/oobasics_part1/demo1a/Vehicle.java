package com.psybergate.grad2021.core.oobasics_part1.demo1a;

public abstract class Vehicle {

  abstract void bike();
}
