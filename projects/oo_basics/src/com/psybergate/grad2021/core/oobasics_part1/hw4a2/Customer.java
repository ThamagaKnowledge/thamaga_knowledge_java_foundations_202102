package com.psybergate.grad2021.core.oobasics_part1.hw4a2;

public class Customer {

  private static final int MINIMUM_AGE = 18;

  private String customerNum;

  private String customerName;

  private String customerSurname;

  private int customerAge;

  private String customerType;

  public Customer(String  customerNum, String customerName, String customerSurname, int customerAge,String customerType) {
    this.customerNum = customerNum;
    this.customerName = customerName;
    this.customerSurname = customerSurname;

    if (!(isValidCustomer(customerAge))){
      System.out.println("Invalid Customer");
    }

    this.customerAge = customerAge;
    this.customerType = customerType;
  }

  public static int getMinimumAge(){
    return MINIMUM_AGE;
  }

  public String getCustomerNum(){
    return this.customerNum;
  }

  public String getCustomerName(){
    return this.customerName;
  }

  public String getCustomerSurname(){
    return this.customerSurname;
  }

  public int getCustomerAge(){
    return this.customerAge;
  }

  public String getCustomerType(){
    return this.customerType;
  }

  public boolean isValidCustomer(int customerAge){
    return customerAge >= MINIMUM_AGE;
  }
}
