package com.psybergate.grad2021.core.oobasics_part1.hw5a;

public class CustomerUtils {

  public static void main(String[] args) {
    Customer customer = new Customer();
    printBalances(customer);
  }

  public static void printBalances(Customer customer) {
    for (Account account : customer.getAccounts()) {
      System.out.println(account.printAccount());
    }
  }
}
