package com.psybergate.grad2021.core.oobasics_part1.ce2a;

public class CurrentAccountUtils {

  public static void main(String[] args) {

    CurrentAccount currentAccount = new CurrentAccount(1,
        "Thamaga", 40000, "Current Acc",
        0.9);

    CurrentAccount currentAccount2 = new CurrentAccount(1,
        "Thamaga", -70000, "Current Acc",
        0.9);

    CurrentAccount currentAccount3 = new CurrentAccount(1,
        "Thamaga", -50000, "Current Acc",
        0.9);

    //currentAccount.isOverdrawn();
    currentAccount.needsToBeReviewed();

 //   currentAccount2.isOverdrawn();
    currentAccount2.needsToBeReviewed();

   // currentAccount3.isOverdrawn();
    currentAccount3.needsToBeReviewed();
  }
}
