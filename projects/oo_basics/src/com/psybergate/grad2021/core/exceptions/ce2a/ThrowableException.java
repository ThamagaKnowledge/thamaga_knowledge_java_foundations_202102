package com.psybergate.grad2021.core.exceptions.ce2a;

public class ThrowableException extends Throwable {

  public ThrowableException(String message) {
    super(message);
  }
}
