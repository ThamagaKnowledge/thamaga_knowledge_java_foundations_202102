package com.psybergate.grad2021.core.exceptions.ce2a;

public class ExtendsError extends Error {

  public ExtendsError(String message) {
    super(message);
  }
}
