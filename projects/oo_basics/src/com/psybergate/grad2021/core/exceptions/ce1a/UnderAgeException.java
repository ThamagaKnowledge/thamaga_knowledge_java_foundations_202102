package com.psybergate.grad2021.core.exceptions.ce1a;

public class UnderAgeException extends Exception {

  private int age;

  public UnderAgeException(String message, int age) {
    super(message);
    this.age = age;
  }

}
