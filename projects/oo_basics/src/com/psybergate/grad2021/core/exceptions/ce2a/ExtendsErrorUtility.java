package com.psybergate.grad2021.core.exceptions.ce2a;

public class ExtendsErrorUtility {

  public static void main(String[] args) {
    throwSomeError();
  }

  public static void throwSomeError() {
    throw new ExtendsError("This Exception Extends Error. ");

    //Error is a Runtime Exception, so there is no
    // need to Propagate it.
    //The Compiler is smart enough to figure that out by itself
  }
}
