package com.psybergate.grad2021.core.exceptions.ce2a;

public class CheckedException extends Exception {

  public CheckedException(String message) {
    super(message);
  }
}
