package com.psybergate.grad2021.core.oobasics_part2.hw2a;

public class RectangleUtils {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(100,50);
        Rectangle rectangle1 = rectangle;
        Rectangle rectangle2 = new Rectangle(400,100);

        System.out.println("is rectangle identical to rectangle1 ? " + (rectangle == rectangle1));
        System.out.println("is rectangle identical to rectangle1 ? " + (rectangle == rectangle2));
        System.out.println("rectangle.equals(rectangle2) = " + rectangle.equals(rectangle2));
    }

}
