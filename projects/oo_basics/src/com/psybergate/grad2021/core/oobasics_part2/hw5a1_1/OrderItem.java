package com.psybergate.grad2021.core.oobasics_part2.hw5a1_1;

public class OrderItem {

  private Product product;

  private int quantity;

  public OrderItem(Product product, int quantity) {
    this.product = product;
    this.quantity = quantity;
  }

  public int getQuantity() {
    return quantity;
  }


  public double calcPricePerUnit() {
    return (getQuantity() * product.getProductPrice());
  }


  public String printItem() {
    return "OrderItem{" +
        "product=" + product.printProduct() +
        ", quantity=" + quantity +
        '}';
  }
}
