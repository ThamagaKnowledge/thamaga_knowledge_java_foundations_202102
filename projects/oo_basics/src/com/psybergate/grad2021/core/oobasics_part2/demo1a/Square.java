package com.psybergate.grad2021.core.oobasics_part2.demo1a;

public class Square extends Shape {

    public Square() {
        super(3, 4, 5);
    }

    //If i uncomment the below code, the code won't compile, which shows constructors cannot be overriden
//    public Shape(double length, double width, double height){
//        this.length = length;
//        this.width = width;
//        this.height = height;
//    }

    //This code doesn't compile, which shows that static methods cannot be overriden
    //@Override
    public static void printSomething() {
        System.out.println("I am a static method and, i cannot be overriden");
    }

    //This will never be possible because private methods are not inherited, hence cannot be overriden,
    //the compiler will treat this a completely new defined method ?
    //@Override
    private void print() {
        System.out.println("I am a result of overrding a method");
    }

    //This does not compile because the scope must be the same or broader than the parent's scope
    //Child scope cannot be private
//    @Override
//    private double calcualteArea() {
//        return super.calcualteArea();
//    }
}
