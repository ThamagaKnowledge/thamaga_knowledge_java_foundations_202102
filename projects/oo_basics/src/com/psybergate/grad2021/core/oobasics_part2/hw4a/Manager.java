package com.psybergate.grad2021.core.oobasics_part2.hw4a;

public class Manager extends Employee{

  public Manager() {
    super("employeeId", "employeeName", "employeeSurname", 34, "employeeAddress");
  }

  public static void displaySomeMessages(){
    System.out.println("I am a Manager static method, i cannot be invoked Polymorphically");
  }


}
