package com.psybergate.grad2021.core.oobasics_part2.demo2a;

import java.util.ArrayList;

public class AccountUtils {
    public static void main(String[] args) {

        ArrayList<CurrentAccount> arrayList = new ArrayList<>();

        CurrentAccount currentAccount = new CurrentAccount(1234, 20.00);
        CurrentAccount currentAccount1 = currentAccount;
        CurrentAccount currentAccount2 = new CurrentAccount(1234, 0.00);
        CurrentAccount currentAccount3 = new CurrentAccount(1235, 20);

        arrayList.add(currentAccount);
        arrayList.add(currentAccount2);
        arrayList.add(currentAccount3);

       // Showing object identity using the "==" operator.
        System.out.println("is currentAccount identical to currentaccount1 ? " + (currentAccount == currentAccount1));
        //Testing if the list contains some object.
        System.out.println("arrayList.contains(currentAccount2) = " + arrayList.contains(currentAccount2));

        System.out.println("currentAccount.equals(currentAccount2) = " + currentAccount.equals(currentAccount2));
        System.out.println("currentAccount.equals(currentAccount3) = " + currentAccount.equals(currentAccount3));
    }
}
