package com.psybergate.grad2021.core.oobasics_part2.ce2a;

public class PassValue {
    public static void main(String args[]) {
        Animal1 a1 = new Animal1("Lion");

        System.out.println("Before Modify: " + a1.getName());
        modify(a1);
        System.out.println("After Modify: " + a1.getName());
    }

    public static void modify(Animal1 animal) {
        animal.setName("Tiger");
    }

}

class Animal1 {
    String name;

    public Animal1(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
