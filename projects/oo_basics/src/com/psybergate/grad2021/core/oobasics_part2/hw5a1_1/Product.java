package com.psybergate.grad2021.core.oobasics_part2.hw5a1_1;

public class Product {

  private String productName;

  private double productPrice;

  public Product(String productName, double productPrice) {
    this.productName = productName;
    this.productPrice = productPrice;
  }

  public double getProductPrice(){
    return this.productPrice;
  }

  public String printProduct() {
    return "Product{" +
        "productName='" + productName + '\'' +
        ", productPrice=" + productPrice +
        '}';
  }
}
