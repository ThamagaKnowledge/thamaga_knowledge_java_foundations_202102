package com.psybergate.grad2021.core.oobasics_part2.ce1a.code2;

public class CurrentAccount extends Account {

    private double interest;

    private double deposit;

    public CurrentAccount(int accountNum, double balance, double interest, double deposit) {
        super(accountNum, balance);
        this.interest = interest;
        this.deposit = deposit;

        System.out.println("Interest : " + interest);
        System.out.println("deposit : " + deposit);
    }
}

