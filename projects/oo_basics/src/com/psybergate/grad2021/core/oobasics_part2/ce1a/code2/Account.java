package com.psybergate.grad2021.core.oobasics_part2.ce1a.code2;

public class Account {

    private int accountNum;
    protected double balance;

    public Account(int accountNum, double balance) {
        this.balance = balance;
        this.accountNum = accountNum;

        System.out.println("Account Number :  " + accountNum);
        System.out.println("Balance :  " + balance);
    }

}
