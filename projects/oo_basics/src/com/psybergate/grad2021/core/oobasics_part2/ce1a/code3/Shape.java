package com.psybergate.grad2021.core.oobasics_part2.ce1a.code3;

public class Shape {

    protected double length;

    protected double width;

    protected double height;

    public Shape(double length, double width, double height) {
        this.length = length;
        this.width = width;
        this.height = height;
    }

}
