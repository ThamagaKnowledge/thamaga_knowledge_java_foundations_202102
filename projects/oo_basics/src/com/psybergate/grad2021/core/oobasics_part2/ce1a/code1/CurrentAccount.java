package com.psybergate.grad2021.core.oobasics_part2.ce1a.code1;

public class CurrentAccount extends Account {

	// The Code does not compile because I did not explicitly call a specific
	// Constructor on the SuperClass (i.e super(accountNum).

	public CurrentAccount() {
		super(45);
	}
}
