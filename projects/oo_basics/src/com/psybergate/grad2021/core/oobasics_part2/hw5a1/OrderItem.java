package com.psybergate.grad2021.core.oobasics_part2.hw5a1;

public class OrderItem {

  private String productName;

  private int quantity;

  private double price;

  public OrderItem(String productName, int quantity, double price) {
    this.productName = productName;
    this.quantity = quantity;
    this.price = price;
  }

  public int getQuantity(){
    return quantity;
  }

  public double getPrice() {
    return price;
  }

  public double calcPricePerUnit(){
    return (getQuantity() * getPrice());
  }

  public String printOrderedItems() {
    return "OrderItem{" +
        "productName='" + productName + '\'' +
        ", quantity=" + quantity +
        ", price=" + price +
        '}';
  }
}
