package com.psybergate.grad2021.core.oobasics_part2.ce1a.code3;

public class Rectangle extends Shape {

    //Without this constructor, the code won't compile. which shows constructors cannot be inherited.
    public Rectangle() {
        super(3.4, 2.3, 3.3);
    }
}
