package com.psybergate.grad2021.core.oobasics_part2.hw5a1;

public class Customer {

  private String customerId; //Unique //Is this Object variable even necessary ?

  private String customerName;

  private String customerSurname;

  private String customerEmailAddress;

  private String customerAddress;

  private String customerType;


  public Customer(String customerId, String customerName, String customerSurname, String customerEmailAddress, String customerAddress, String customerType) {
    this.customerId = customerId;
    this.customerName = customerName;
    this.customerSurname = customerSurname;
    this.customerEmailAddress = customerEmailAddress;
    this.customerAddress = customerAddress;
    this.customerType = customerType;
  }

  public String printCustomer() {
    return

        "customerName : " + customerName + '\'' +
        " customerSurname : " + customerSurname + '\'' +
        " customerType : " + customerType + '\'' ;

  }
}
