package com.psybergate.grad2021.core.oobasics_part2.hw4a;

public class Employee {

  private String employeeId;

  private String employeeName;

  private String employeeSurname;

  private int employeeAge;

  private String employeeAddress;

  public Employee(String employeeId, String employeeName, String employeeSurname, int employeeAge, String employeeAddress) {
    this.employeeId = employeeId;
    this.employeeName = employeeName;
    this.employeeSurname = employeeSurname;
    this.employeeAge = employeeAge;
    this.employeeAddress = employeeAddress;
  }

  public static void displaySomeMessage(){
    System.out.println("I am an Employee static method, I am supposed to be inherited");
  }


}
