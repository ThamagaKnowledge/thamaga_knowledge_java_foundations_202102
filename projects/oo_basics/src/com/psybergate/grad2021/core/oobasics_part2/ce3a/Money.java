package com.psybergate.grad2021.core.oobasics_part2.ce3a;

import java.math.BigDecimal;

public class Money {

    BigDecimal amount;

    public Money(double amount) {
        this.amount = new BigDecimal(amount);
    }

    public Money add(Money money) {
        return new Money(this.amount.add(money.amount).doubleValue());
    }

}

class MoneyUtils{
    public static void main(String[] args) {
      BigDecimal amount = new BigDecimal(10);
      BigDecimal money = new BigDecimal(5);
        System.out.println("amount.add(amount) = " + amount.add(money));
    }
}
