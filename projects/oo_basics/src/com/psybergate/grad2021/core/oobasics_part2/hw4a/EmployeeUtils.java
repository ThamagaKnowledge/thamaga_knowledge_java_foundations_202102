package com.psybergate.grad2021.core.oobasics_part2.hw4a;

public class EmployeeUtils {

  public static void main(String[] args) {
    Manager manager = new Manager();
    //Shows Static method can be Inherited.
    Manager.displaySomeMessage();

    //Shows Static Method cannot be invoked Polymorphically
    Employee employee = new Manager();
    employee.displaySomeMessage();
  }
}
