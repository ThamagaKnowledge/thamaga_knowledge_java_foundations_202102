package com.psybergate.grad2021.core.oobasics_part2.hw3a;

public class CustomerUtils {
  public static void main(String[] args) {
    Customer customer = new Customer("1","name1","surname1",21,"Person");
    Customer customer1 = customer;
    Customer customer2 = new Customer("1","name2","surname2",22,"Person");
    Customer customer3 = new Customer("3","name3","surname3",23,"Company");

    System.out.println("(customer == customer1) = " + (customer == customer1));
    System.out.println("customer.equals(customer2) = " + customer.equals(customer2));
  }
}
