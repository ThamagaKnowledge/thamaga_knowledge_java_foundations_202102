package com.psybergate.grad2021.core.oobasics_part2.demo1a;

public class Shape {
    private double length;
    private double width;
    private double height;

    public Shape(double length, double width, double height){
        this.length = length;
        this.width = width;
        this.height = height;
    }

    public static void printSomething(){
        System.out.println("I am a static method and, i cannot be overriden");
    }

    private void print(){
        System.out.println("I am suppsoed to be overriden");
    }

    public double calcualteArea(){
        return length * width;
    }






}
