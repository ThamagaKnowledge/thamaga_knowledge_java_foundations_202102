package com.psybergate.grad2021.core.oobasics_part2.demo2a;

import java.util.ArrayList;

public class CurrentAccount extends Account {
  private int accountNum; //Unique

  private double balance;

  ArrayList<CurrentAccount> arrayList = new ArrayList<>();

  public CurrentAccount(int accountNum, double balance) {
    super("Thamaga", "Current Account");
    this.accountNum = accountNum;
    this.balance = balance;
  }

  public int getAccountNum() {
    return this.accountNum;
  }

  public boolean contains(CurrentAccount currentAccount) {
    if (arrayList.isEmpty()) {
      return false;
    }
    if (arrayList.contains(currentAccount)) {
      return true;
    }

    return false;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) { //Identity Test
      return true;
    }
    if (obj == null) { //Non-Nullabilty
      return false;
    }
    if (!(obj instanceof CurrentAccount)) {
      return false;
    }

    CurrentAccount currentAccount = (CurrentAccount) obj;
    //return this.accountNum.equals(currentAccount); //State Equivalence

    if ((this.accountNum == currentAccount.accountNum)) {
      return true;
    }

    return false;

  }

}
